export function getRandomColor() {
    const R = Math.floor(Math.random() * 255).toString(16)
    const G = Math.floor(Math.random() * 255).toString(16)
    const B = Math.floor(Math.random() * 255).toString(16)
    return `#${R}${G}${B}`
  }