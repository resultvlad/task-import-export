import { getRandomColor } from "./utils"

export default function initApp() {
    console.log('Hello world');

    const changeColorButton = document.createElement('button')
    changeColorButton.className = 'button'
    changeColorButton.textContent = 'Изменить цвет страницы'
    document.body.append(changeColorButton)
    
    changeColorButton.addEventListener('click', () => {
        document.body.style.background = getRandomColor()
    })
}